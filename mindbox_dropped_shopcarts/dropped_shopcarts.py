import asyncio
import datetime

import aiohttp
import aioodbc
import uvloop
from collections import defaultdict

from common.imports import import_any
from common.logging import log
from dwh.client import ConnectionToODBC
from settings.dwh import maxsize, minsize, dsn
from dwh.utils import full_sap_code
from rtom.client import ClientRTOM
from mindbox.client import ClientMindBox

json = import_any('ujson', 'simplejson', 'json')


# NB!: Time of the receiving data from dwh is before 7 a.m.
async def dropped_checks_from_dwh(loop, pool):
    """
    get all sap_codes(products) with Dropped ShopCarts from DWH
    """
    dwh = ConnectionToODBC(loop)
    dwh.pool = pool

    query = '''SELECT
        ConsolidateCode AS PosCode,
        ClientGUID,
        OrderID,
        CodeNum AS SapCode,
        TotalPrice=Price-Discount
    FROM dwh.export.v_RtomRejectedOrder
    ORDER BY OrderID;'''

    if not await dwh.check_connect():
        emessage = 'Not connect ot DWH...'
        log.error(emessage)
        assert False, emessage

    return await dwh.send_query(query)


def de_prepare_data(data):
    """ preparing data for sending to discount_engine
    """

    result = {}
    orders = defaultdict(list)

    # aggregating orders by order_id
    for pos_code, guid, order_id, sap_code, total_price in data[:20]:
        orders[order_id].append([full_sap_code(sap_code), pos_code, guid])

    for order_id, products in orders.items():
        client_uuid = {guid for sap_code, pos_code, guid in products}
        pos_code = {pos_code for sap_code, pos_code, guid in products}
        assert len(pos_code) == len(client_uuid) == 1, (
            'More one type "pos_code" of the shop or "guid" of the client for one order_id. '
            'This is bad, check to get the data from DWH '
            'for order_id = {order_id}\n, '
            'pos_codes = {pos_codes}\n,'
            'guid_clients = {client_guids}.'.format(order_id=order_id, pos_codes=pos_code, client_guids=client_uuid))

        result[order_id] = {'client_uuid': client_uuid.pop().lower(),
                            'pos_code': pos_code.pop(),
                            'sap_codes': [sap_code for sap_code, pos_code, guid in products]}
    return result


async def main(loop):
    """ It's basic logic this script 
    """

    # get soft payments checks from DWH
    async with aioodbc.create_pool(loop=loop, dsn=dsn, minsize=minsize, maxsize=maxsize) as odbc_pool:
        dropped_shop_checks = await dropped_checks_from_dwh(loop=loop, pool=odbc_pool)

    # aggregating data
    aggregated_orders = de_prepare_data(dropped_shop_checks)

    async with aiohttp.ClientSession() as session:
        rtom_client = ClientRTOM(loop=loop, session=session, production=False)
        mbx_client = ClientMindBox(loop=loop, session=session, production=False)

        for order_id, order_data in aggregated_orders.items():
            client_uuid = order_data['client_uuid']
            pos_code = order_data['pos_code']
            sap_codes = order_data['sap_codes']
            offer_json = {'sap_codes': sap_codes, 'pos_code': pos_code}
            valid_to = str(datetime.datetime.now().date() + datetime.timedelta(days=5))

            # create offer for whole order
            offer = await rtom_client.create_offer(
                client_uuid=client_uuid, json_data=offer_json, valid_to=valid_to)
            assert offer['data'], 'Return empty offer {}'.format(offer)
            offer_id = offer['data'][0]['id']

            # calculating discount for whole order
            discount_data = await rtom_client.calculate_discount(client_uuid, offer_json)

            # canceling offers with zero discount
            if rtom_client.is_order_with_zero_discount(discount_data):
                log.warning('Order with ID=%s have zero discount for all products.\n'
                            'Order data: %s' % (order_id, order_data))
                await rtom_client.offer_cancel(offer_id)
                continue

            # send order to a mindbox.ru (zoloto585.directcrm.ru)
            is_sending = await mbx_client.send_request(client_uuid, discount_data)

            if not is_sending:
                # cancel offer in Discount Engine
                await rtom_client.offer_cancel(offer_id=offer_id)


if __name__ == '__main__':
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
    ioloop = asyncio.get_event_loop()

    ioloop.run_until_complete(main(ioloop))

    # see: https://docs.aiohttp.org/en/stable/client_advanced.html#graceful-shutdown
    ioloop.run_until_complete(asyncio.sleep(0))
    ioloop.close()

import time


def initiliaze_counter():
    return time.time()


# TODO: need to deal
# def atimer(log):
#     async def wrapper(f):
#         async def tmp(*args, **kwargs):
#             t = time.time()
#             res = await f(*args, **kwargs)
#
#             message = ("Время выполнения '%s' функции: %f" % (f.__name__, time.time() - t))
#             print(message) if log is None else log.info(message)
#             return res
#         return await tmp
#     return wrapper


def timer(log):
    def wrapper(f):
        def tmp(*args, **kwargs):
            t = time.time()
            res = f(*args, **kwargs)
            message = ("Время выполнения '%s' функции: %f" % (f.__name__, time.time() - t))
            print(message) if log is None else log.info(message)
            return res

        return tmp

    return wrapper

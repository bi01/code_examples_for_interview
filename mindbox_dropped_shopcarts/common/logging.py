import logging
import sys

from raven import Client
from raven.handlers.logging import SentryHandler
from raven.conf import setup_logging

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
log.addHandler(ch)

sentry_mindbox_project = 'http://ad052124ca474d62ad32bac9d61edbfa:9273901b07994b1387f0772aebf8d452@127.0.0.1:8080/11'
client = Client(sentry_mindbox_project, auto_log_stacks=True)
handler = SentryHandler(client)

setup_logging(handler)

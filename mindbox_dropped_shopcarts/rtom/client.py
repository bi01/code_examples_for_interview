from common.imports import import_any

json = import_any('ujson', 'simplejson', 'json')


class ClientRTOM(object):
    def __init__(self, loop, session, production=False):
        self.loop = loop
        self.session = session
        self.production = production
        self._prod_host = 'http://prod.int'
        self._qa_host = 'http://dev.int'
        self.port = 30000
        self.host = self._prod_host if self.production else self._qa_host
        self.api_url = '{host}:{port}/{api}'.format(host=self.host, port=self.port, api='v1')

    @staticmethod
    def is_order_with_zero_discount(data):
        return all(map(lambda x: x == 0, [item['discount'] for item in data]))

    async def create_offer(self, client_uuid, json_data, valid_to) -> dict:
        """ Creating offer for specified order 
        """

        url = (
            '{api_url}/clients/{client_uuid}'
            '/offers/generate/model_drop_cart/1.0/default/'
            '?offer_type=regular&valid_to={valid_to}'.format(api_url=self.api_url,
                                                             client_uuid=client_uuid,
                                                             valid_to=valid_to))

        async with self.session.post(url=url, json=json_data) as resp:
            try:
                offer = await resp.json()
                assert resp.status in {200, 201}, ('HTTPCode is not equal 200, 201. '
                                                   'Has been the error by creating an offer in DiscountEngine\n'
                                                   'JSON_DATA: {} \n'
                                                   'RESPONSE: {}'.format(json_data, offer))
            except AssertionError:
                raise
        return offer

    async def calculate_discount(self, client_uuid, data_json) -> list:
        """ Get discount for the received DroppedShopCart
        """

        url = ('{api_url}/clients/{client_uuid}/offers/apply/'
               '?register_purchase=false'
               '&as_client=false'
               '&deactivate_applied_offers=false'.format(api_url=self.api_url, client_uuid=client_uuid))

        async with self.session.post(url=url, json=data_json) as resp:
            try:
                received_data = await resp.json()
                assert resp.status == 200, ('HTTPCode is not equal 200. '
                                            'Has been the error by calculating of the discount in DiscountEngine\n'
                                            'JSON_DATA: {} \n'
                                            'RESPONSE: {}'.format(data_json, received_data))
            except AssertionError:
                raise
        return received_data['data']

    async def offer_cancel(self, offer_id):
        data_json = {'active': False}
        url = '{api_url}/offers/{id}/'.format(api_url=self.api_url, id=offer_id)
        async with self.session.patch(url=url, json=data_json) as resp:
            try:
                received_data = await resp.json()
                assert resp.status == 200, ('HTTPCode is not equal 200. '
                                            'Has been the error by calculating of the discount in DiscountEngine\n'
                                            'JSON_DATA: {} \n'
                                            'URL: {} \n'
                                            'RESPONSE: {}'.format(data_json, url, received_data))
            except AssertionError:
                raise
        assert received_data['data']['active'] is False, (
            'Hmmm... This is strange, but a status for current offer_id "{}" not been changing'.format(offer_id))

import asyncio
import aiohttp
import uvloop


async def main(loop):
    email = 'Kuznetsov.Gennady@zoloto585.ru'
    password = 'ougxAx{Q'
    payload = {
        'UserName': email,
        'Password': password,
    }

    url = ('https://zoloto585.directcrm.ru/operations-log'
           '?filter=and(operationSystemName(zoloto585SetCustomerRTMOCartList))'
           'and(endpointExternalId(zoloto585.processing.test))')
    async with aiohttp.ClientSession() as session:
        async with session.post(url=url, allow_redirects=True, data=payload) as resp:
            print(await resp.text())


if __name__ == '__main__':
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
    ioloop = asyncio.get_event_loop()

    ioloop.run_until_complete(main(ioloop))

    # see: https://docs.aiohttp.org/en/stable/client_advanced.html#graceful-shutdown
    ioloop.run_until_complete(asyncio.sleep(0))
    ioloop.close()

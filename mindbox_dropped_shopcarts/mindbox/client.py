from settings import mindbox
from mindbox.request_structures import MindBoxData


class ClientMindBox(object):
    def __init__(self, loop, session, production=False):
        self.loop = loop
        self.session = session
        self.mindbox = mindbox.get_access(production)

    async def send_request(self, client_uuid, product_list) -> bool:
        url = ('https://api.mindbox.ru/v3/operations/async?endpointId={endpoint}&operation={operation}'.format(
            endpoint=self.mindbox['endpoint'], operation=self.mindbox['operations']['make_discount']))

        data_json = MindBoxData(client_uuid, product_list).generate_json()

        mb_headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Mindbox secretKey="{}"'.format(self.mindbox['api_key'])
        }

        async with self.session.post(url=url, headers=mb_headers, json=data_json) as resp:
            try:
                received_data = await resp.json()
                assert resp.status == 200, ('HTTPCode is not equal 200. '
                                            'Has been the error by calculating of the discount in DiscountEngine\n'
                                            'JSON_DATA: {} \n'
                                            'RESPONSE: {}'.format(data_json, received_data))

                operation_status = received_data['status']
                assert operation_status == 'Success', ('Operation status is not have "SUCCESS"\n'
                                                       'JSON_DATA: {} \n'
                                                       'RESPONSE: {}'.format(data_json, received_data))
            except AssertionError as e:
                print('An error occurred: {}'.format(e))
                return False
        return False

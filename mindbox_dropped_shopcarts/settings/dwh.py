import pyodbc

minsize = 1
maxsize = 2

DWH_HOST = '127.0.0.1'
DWH_DB = 'db'
DWH_USER = 'user'
DWH_PASSWORD = 'abc123'

dsn = (
    'DRIVER={odbc_lib};'
    'SERVER={dwh_host};'
    'DATABASE={dwh_db};'
    'UID={dwh_user};'
    'PWD={dwh_passwd}'.format(
        odbc_lib=pyodbc.drivers()[0],
        dwh_host=DWH_HOST,
        dwh_db=DWH_DB,
        dwh_user=DWH_USER,
        dwh_passwd=DWH_PASSWORD))

def get_access(prod=False):
    return {
        'api_key': 'aaaaaaaaaaaa' if prod else 'bbbbbbbbbbbbb',
        'endpoint': 'prod.processing' if prod else 'dev.processing.test',

        'operations': {
            'make_discount': 'SetCustomerRTMOCartList',
            'create_user': 'CustomerRegistrationRTOM',
            'update_user': 'CustomerEditProfileRTOM',
            'create_order': 'CreateOrderRTOM',
            'create_auth_order': 'CreateOrderRTOMAuth',
            'execute_rtom': 'OrderChangeRTOM',
            'execute_interest': 'CustomerViewProductRTOM',
        }
    }

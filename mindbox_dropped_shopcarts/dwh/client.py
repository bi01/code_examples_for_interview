class ConnectionToODBC(object):
    def __init__(self, loop):
        self.loop = loop
        # NB! You must  set up context manager for the pool
        self.pool = None

    async def send_query(self, query):
        async with self.pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(query)
                result = await cur.fetchall()
        return result

    async def check_connect(self):
        result = await self.send_query('SELECT 1;')
        return bool(result)

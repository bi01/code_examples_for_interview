import uuid


def uuid_client(binary_data):
    return str(uuid.UUID(binary_data.hex()))


def full_sap_code(sap_code):
    return str(sap_code).rjust(18, '0')

#!/bin/bash

rm -rf ./.venv
mkdir -p ./.venv
python3 -m virtualenv -p python3 --always-copy ./.venv

source ./.venv/bin/activate

pip install -r requirements.txt


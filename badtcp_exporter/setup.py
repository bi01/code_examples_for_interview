from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='badtcp_exporter',
    version='0.0.4',
    author='Oleg Biryukov',
    author_email='oleg.biruykov@gmail.com',
    url='https://bitbucket.org/bi01',
    description="Counter total tcp bad requeset for specified hosts and ports",
    long_description=long_description,
    packages=find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Linux",
    ],

    install_requires=['prometheus-client>=0.3.1']
)

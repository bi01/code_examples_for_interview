#!/usr/bin/env python3

import datetime
import socket
import time

from prometheus_client import Gauge, start_http_server

PORT_EXPORTER = 9999


def is_opened_tcp_port(item='ya.ru:80', timeout=3):
    host, port = item.split(':')
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(timeout)
    try:
        result = sock.connect_ex((host, int(port)))
    except:
        return False
    finally:
        sock.close()

    return result == 0


def get_current_time_in_munutes():
    current_time = datetime.datetime.now()
    return current_time - datetime.timedelta(seconds=int(current_time.second))


if __name__ == '__main__':
    local_hostname = socket.gethostname()

    targets = [
        'cizcp47.zoloto585.int:8047',
        'spb-s-infra1-dev.zoloto585.int:80',
        'pl-mrk1.zoloto585.int:9200',
        'pl-mrk2.zoloto585.int:9200',
        'pl-mrk3.zoloto585.int:9200',
        'pl-mrk4.zoloto585.int:5432',
        'ya.ru:443',
        'google.com:443',
    ]

    g = Gauge('tcp_errors_in_minute_total', 'Total tcp errors for specified fqdn and port',
              ['send_from', 'send_to'])

    for target in targets:
        label = target.replace(':', '_').replace('.', '_').replace('-', '_')
        g.labels(send_from=local_hostname, send_to=target).set(0)

    start_http_server(PORT_EXPORTER)

    start = get_current_time_in_munutes()
    while True:
        time.sleep(1)

        if 60 <= (start.now() - start).total_seconds():
            start = get_current_time_in_munutes()
            for target in targets:
                g.labels(send_from=local_hostname, send_to=target).set(0)

        for target in targets:
            if not is_opened_tcp_port(target):
                g.labels(send_from=local_hostname, send_to=target).inc()

#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import socket
import subprocess
from datetime import datetime, timedelta
from getpass import getpass, getuser

import teamcity_client.client
from teamcity_client.rest import TeamcityRestApi

HOST = 'teamcity.prod.ru'


def hostname(h=None):
    return (h if h else socket.gethostname()).split('.', 1)[0]


def get_agent_by_name(client, host):
    for x in client.agents:
        if host in x.name:
            return x
    return


def parse_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        '-d',
        '--downtime',
        type=float,
        default=3,
        help='In this is time the agent will disabled in minutes')
    parser.add_argument(
        '-l',
        '--login',
        default=getuser(),
        help='Login to auth on TeamCity server')
    parser.add_argument(
        '-a',
        '--hostname_agent',
        default=None,
        help='By default it is hostname local server or specify agent hostname'
    )
    parser.add_argument('-p', '--password')
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = parse_args()
    password = args.password or getpass('Password for {}: '.format(args.login))

    api = TeamcityRestApi(HOST, auth=(args.login, password))
    client = teamcity_client.client.TeamcityClient(api)
    enable_at = datetime.now() + timedelta(minutes=args.downtime)
    agent = client.agents[hostname(args.hostname_agent)]
    agent.disable(
        'Turn off agent on for {} minutes'.format(args.downtime),
        enable_at=enable_at)


# Code examples for interviews

## TL; DR;
 - [badtcp_exporter](badtcp_exporter)
 - [github_trending](github_trending)
 - [mindbox_dropped_shopcarts microservice](mindbox_dropped_shopcarts)
 - [parser_email_from_site](parser_email_from_site)
 - [reboot_mac_agents](reboot_mac_agents)

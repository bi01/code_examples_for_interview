import asyncio
import re

import aiohttp
import async_timeout
from openpyxl import load_workbook


def load_data_from_xlsx(path_xlsx) -> list:
    wb = load_workbook(filename=path_xlsx, read_only=True)
    ws = wb['sites_list']
    result = sorted({row[0].value for row in ws.rows})
    wb.close()

    return result


async def fetch(session, url):
    async with async_timeout.timeout(3):
        async with session.get(url) as response:
            return await response.text()


async def get_emails(url):
    async with aiohttp.ClientSession() as session:
        try:
            html = await fetch(session, url)
        # TODO: url -> https|http://url
        except Exception as e:
            print(f'Error occurred and url: {url} has been missed\nError: {e}')
            return

    return url, set(re.findall('[a-zA-Z0-9_\-\.]+?\@[a-zA-Z0-9_\-\.]+?\.\w{2,10}', html))


async def main():
    sites = load_data_from_xlsx('./sites.xlsx')
    for url in sites:
        result = await get_emails(url)
        print(result)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    loop.close()
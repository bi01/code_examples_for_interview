from pprint import pprint as pp

import requests
from bs4 import BeautifulSoup
from sqlalchemy import create_engine, Column, Integer, String, Sequence
from sqlalchemy.engine.url import URL
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

db_uri = {
    'drivername': 'sqlite',
    'database': 'db.sqlite',
}
engine = create_engine(URL(**db_uri), echo=True)
Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
session = Session()
Base = declarative_base()


class Repositories(Base):
    __tablename__ = 'repositories'

    id = Column(Integer, Sequence('repo_id_seq'), primary_key=True)
    author = Column(String(100))
    repo_name = Column(String(150), unique=True)
    url = Column(String(200))
    star_count = Column(Integer)

    def __init__(self, author, repo_name, url, star_count):
        self.author = author
        self.repo_name = repo_name
        self.url = url
        self.star_count = star_count

    def __repr__(self):
        return ("<Repository(author='%s', reponame='%s', "
                "url='%s', star_count='%s')>") % (
                   self.author, self.repo_name, self.url, self.star_count)


def get_records():
    result = []
    full_url = 'https://github.com/trending'
    headers = {
        "User-Agent": ("Mozilla/5.0 (X11; Linux x86_64) "
                       "AppleWebKit/537.36 (KHTML, like Gecko) "
                       "Chrome/51.0.2704.106 Safari/537.36")
    }
    with requests.Session() as s:
        r = requests.get(full_url, headers)

    soup = BeautifulSoup(r.content, "lxml")
    info_repos = soup.select('div.explore-content > ol.repo-list > '
                             'li > div.d-inline-block.col-9.mb-1 > h3 > a')
    for repo in info_repos:
        author_repo_info = repo.get_text().split('/', 1)

        author = author_repo_info[0].replace('/', '').strip()
        repository = author_repo_info[1].replace('/', '').strip()
        url = 'https://github.com' + repo.get('href')
        star_count = repo.parent.parent.parent.select(
            'div.f6.text-gray.mt-2 > a')[0].get_text().replace(',',
                                                               '').strip()
        result.append((author, repository, url, int(star_count)))

    return result


def upload_data_to_db(data, s=session):
    s.add_all([Repositories(*x) for x in data])
    s.commit()


def create_examples(s=session):
    print('-=-=-= Creating our a test repository -=-=-')

    new_repo = Repositories(
        author='Author',
        repo_name='SuperRepo',
        url='https://github.com/Author/super_repo',
        star_count=9999,
    )
    s.add(new_repo)
    s.commit()

    print(' -=-=- Show created record -=-=- ')
    pp(s.query(Repositories).filter_by(author='Author').one())

    print(' -=-=- show all records -=-=- ')
    pp(list(s.query(Repositories).all()))

    print(' -=-=- Total record in table -=-=- ')
    pp(s.query(Repositories).count())


def select_examples(s=session):
    print(
        ' -=-=- Show TOP5 repo with maximux star_count (LIMIT and ORDER by DESC) -=-=- ')
    for x in s.query(Repositories).order_by(Repositories.star_count)[::-1][:5]:
        pp(x)

    print(' -=-=- Double filters -=-=- ')
    pp(
        s.query(Repositories).
            filter(Repositories.repo_name == 'SuperRepo').
            filter(Repositories.author == 'Author').
            one()
    )


def update_examples(s=session):
    print(' -=-=- Update our record -=-=- ')
    res = s.query(Repositories).filter(Repositories.star_count == 9999)
    pp(list(res))

    s.query(Repositories).filter(Repositories.star_count == 9999).update(
        {Repositories.author: 'SuperAuthor'})

    res = s.query(Repositories).filter(Repositories.star_count == 9999)
    pp(list(res))


def delete_examples(s=session):
    print(' -=-=- Delete our record -=-=- ')

    s.query(Repositories).filter(Repositories.star_count == 9999).delete()

    res = s.query(Repositories).filter(
        Repositories.author.like('%Author%')).one_or_none()

    pp(res)


if __name__ == '__main__':
    info_by_repos = get_records()

    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)

    upload_data_to_db(info_by_repos)

    create_examples()
    select_examples()
    update_examples()
    delete_examples()

# parsing github.com trending repositories

### TL;DR; 

Parsing a list of the trending repositories from
https://github.com/trending and put him to local SQLite

This is project has I made as the example of use
ORM technology


### Technology stack used

 - Python3
 - requests
 - beatifulsoup (bs4)
 - sqlalchemy ORM
 - sqlite3
